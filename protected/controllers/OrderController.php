<?php

class OrderController extends Controller
{
	public $layout='//layouts/column2';


	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}


	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'update'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionUpdate($id=null){
        if($id == null){
            $model = new Order();

        }
        else if( !$model=Product::model()->findByPk($id) )
            throw new CHttpException(404,'The requested page does not exist!!!');
        //добавить правильное исключение в AJAX??
        $this->performAjaxValidation($model);
        if(isset($_POST['Order'])){
            $model->attributes=$_POST['Order'];

            if($model->save()){
            	if (Yii::app()->request->isAjaxRequest){
					$this->redirect(array('/product/index', 'category_id'=>$model->product->category_id ));
            	}
            	else {
            		Yii::app()->session['order'] = true;
					$this->redirect(array('/product/index', 'category_id'=>$model->product->category_id ));
            	}
            }
        }
        if(Yii::app()->request->isAjaxRequest)
        	$this->renderPartial('update',array('model'=>$model), false, true);
        else
        	$this->render('update',array('model'=>$model));

        // $this->render('update',array('model'=>$model));
    }


	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Order');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionAdmin()
	{
		$model=new Order('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Order']))
			$model->attributes=$_GET['Order'];

		$this->render('admin',array('model'=>$model,));
	}

	public function loadModel($id)
	{
		$model=Order::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='order-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
