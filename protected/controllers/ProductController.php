<?php

class ProductController extends Controller
{

	public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'list'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	public function actionIndex($category_id)
	{
		$criteria=new CDbCriteria;
		$criteria->compare('category_id', $category_id);
		$dataProvider=new CActiveDataProvider('Product',
			['criteria'=>$criteria,
			'pagination'=>array('pageSize'=>false)
		]);

		$this->render('index',array(
				'dataProvider'=>$dataProvider,
				'category_id' => $category_id,
			));
	}


	public function actionView($id)
	{
		$this->render('view',array('model'=>$this->loadModel($id)));
	}


    public function actionUpdate($id=null, $category_id){
        // в зависимости от аргумента создаем модель или ищем уже существующую
        if($id == null){
            $model = new Product();
        	$model->category_id=$category_id;
        }
        else if( !$model=Product::model()->findByPk($id) )
            throw new CHttpException(404,'The requested page does not exist!!!');

        if(isset($_POST['Product'])){
            $model->attributes=$_POST['Product'];
            if($model->save()){
                $this->redirect(array('/product/view','id'=>$model->id));
            }
        }

        $this->render('update',array('model'=>$model));
    }


	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	public function actionAdmin()
	{
		$model=new Product('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Product']))
			$model->attributes=$_GET['Product'];

		$this->render('admin',array('model'=>$model,));
	}


	public function loadModel($id)
	{
		$model=Product::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='product-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
