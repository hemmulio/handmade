<?php

class FotoController extends Controller
{

	public $layout='//layouts/column2';


	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}


	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete', 'add'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


    public function actionUpdate($id=null, $product_id){
        // в зависимости от аргумента создаем модель или ищем уже существующую
        if($id==null){
            $model=new Foto();
        	$model->product_id=$product_id;
        }
        else if(!$model=Foto::model()->findByPk($id))
            throw new CHttpException(404,'The requested page does not exist.');

        if(isset($_POST['Foto'])){
            $model->attributes=$_POST['Foto'];
            if($model->save()){
            	$this->redirect(array('/product/view','id'=>$model->product_id));
            }
        }

        $this->render('update',array('model'=>$model));
        // $this->redirect(array('/product/view','id'=>$model->product_id));
    }


	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	public function loadModel($id)
	{
		$model=Foto::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='foto-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}

