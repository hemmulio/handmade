<?php

class m141019_211714_create_status_on_tbl_order extends CDbMigration
{
/*
	public function up()
	{
	}

	public function down()
	{
		echo "m141019_211714_create_status_on_tbl_order does not support migration down.\n";
		return false;
	}
*/

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->execute("ALTER TABLE `tbl_order`
					ADD `status` tinyint NOT NULL,
					COMMENT=''"
		);
	}

	public function safeDown()
	{
		$this->execute("ALTER TABLE `tbl_order`
					DROP `status`,
					COMMENT='';"
		);
	}

}
