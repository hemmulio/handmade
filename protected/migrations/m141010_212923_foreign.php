<?php

class m141010_212923_foreign extends CDbMigration
{
	// public function up()
	// {
	// }

	// public function down()
	// {
	// 	echo "m141010_212923_foreign does not support migration down.\n";
	// 	return false;
	// }


	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addForeignKey('FK_category', 'tbl_product', 'category_id', 'tbl_category', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('FK_foto', 'tbl_foto', 'product_id', 'tbl_product', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('FK_order', 'tbl_order', 'product_id', 'tbl_product', 'id', 'NO ACTION', 'NO ACTION');

	}

	public function safeDown()
	{
		$this->dropForeignKey('FK_category', 'tbl_product');
		$this->dropForeignKey('FK_foto', 'tbl_foto');
		$this->dropForeignKey('FK_order', 'tbl_order');

	}

}
