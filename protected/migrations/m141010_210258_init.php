		$this->dropTable("{category}");
<?php

class m141010_210258_init extends CDbMigration
{
	public function up()
	{
		$this->execute("CREATE TABLE tbl_category
(
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE tbl_product
(
 	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	category_id INTEGER NOT NULL,
	name VARCHAR(128) NOT NULL,
	description TEXT NOT NULL,
	price DECIMAL NOT NULL,
	sort SMALLINT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE tbl_foto
(
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	product_id INTEGER NOT NULL,
	img_path VARCHAR(128) NOT NULL,
	sort SMALLINT NOT NULL,
	name VARCHAR(128) NOT NULL,
	description TEXT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE tbl_order
(
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	product_id INTEGER NOT NULL,
	create_time DATETIME NOT NULL,
	phone VARCHAR(128) NOT NULL,
	name VARCHAR(128) NOT NULL,
	client_comment TEXT NOT NULL,
	email VARCHAR(128) NOT NULL,
	ip VARCHAR(20) NOT NULL,
	ref_url VARCHAR(950) NOT NULL,
	admin_comment TEXT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");
	}

	public function down()
	{
		$this->dropTable("{{category}}");
		$this->dropTable("{{product}}");
		$this->dropTable("{{foto}}");
		$this->dropTable("{{order}}");
		echo "m141010_210258_init does not support migration down.\n";
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
