<?php

/**
 * This is the model class for table "{{order}}".
 *
 * The followings are the available columns in table '{{order}}':
 * @property integer $id
 * @property integer $product_id
 * @property string $create_time
 * @property string $phone
 * @property string $name
 * @property string $client_comment
 * @property string $email
 * @property string $ip
 * @property string $ref_url
 * @property string $admin_comment
 *
 * The followings are the available model relations:
 * @property Product $product
 */
class Order extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{order}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_id, create_time, phone, name, email, ip, ref_url', 'required'),
			array('product_id', 'numerical', 'integerOnly'=>true),
			array('phone, name', 'length', 'max'=>128),
			array('email', 'email'),
			array('ip', 'length', 'max'=>20),
			array('ref_url', 'length', 'max'=>950),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, product_id, create_time, phone, name, client_comment, email, ip, ref_url, admin_comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'product_id' => 'Product',
			'create_time' => 'Create Time',
			'phone' => 'Phone',
			'name' => 'Name',
			'client_comment' => 'Client Comment',
			'email' => 'Email',
			'ip' => 'Ip',
			'ref_url' => 'Ref Url',
			'admin_comment' => 'Admin Comment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('client_comment',$this->client_comment,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('ref_url',$this->ref_url,true);
		$criteria->compare('admin_comment',$this->admin_comment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
