<?php

/**
 * This is the model class for table "{{product}}".
 *
 * The followings are the available columns in table '{{product}}':
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $description
 * @property string $price
 * @property integer $sort
 *
 * The followings are the available model relations:
 * @property Foto[] $fotos
 * @property Order[] $orders
 * @property Category $category
 */
class Product extends CActiveRecord
{
	public function tableName()
	{
		return '{{product}}';
	}


	public function rules()
	{
		return array(
			array('category_id, name, description, price', 'required'),
			array('category_id, sort', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>128),
			array('price', 'length', 'max'=>10),
			array('id, category_id, name, description, price, sort', 'safe', 'on'=>'search'),
		);
	}


	public function relations()
	{
		return array(
			'fotos' => array(self::HAS_MANY, 'Foto', 'product_id'),
			'orders' => array(self::HAS_MANY, 'Order', 'product_id'),
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
		);
	}


	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Category',
			'name' => 'Name',
			'description' => 'Description',
			'price' => 'Price',
			'sort' => 'Sort',
		);
	}


	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('sort',$this->sort);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function fotoProvider()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('product_id', $this->id);

		return new CActiveDataProvider('Foto', ['criteria'=>$criteria]);
	}

}
