<?php

/**
 * This is the model class for table "{{foto}}".
 *
 * The followings are the available columns in table '{{foto}}':
 * @property integer $id
 * @property integer $product_id
 * @property string $img_path
 * @property integer $sort
 * @property string $name
 * @property string $description
 *
 * The followings are the available model relations:
 * @property Product $product
 */
class Foto extends CActiveRecord
{
	public function tableName()
	{
		return '{{foto}}';
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function rules()
	{
		return array(
			array('product_id', 'required'),
			array('name', 'length', 'max'=>128),
			array('description', 'length', 'max'=>128),
			array('product_id, sort', 'numerical', 'integerOnly'=>true),
			array('img_path', 'file', 'allowEmpty' => true, 'types'=>'jpg, gif, png', 'on' => 'update'),
			array('img_path', 'file', 'allowEmpty' => false, 'types'=>'jpg, gif, png', 'on' => 'insert'),
		);
	}


	public function relations()
	{
		return array(
			'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
		);
	}


	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'product_id' => 'Product',
			'img_path' => 'Img Path',
			'sort' => 'Sort',
			'name' => 'Name',
			'description' => 'Description',
		);
	}


	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('img_path',$this->img_path,true);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, ['criteria'=>$criteria]);
	}

	protected function beforeSave(){
		if(!parent::beforeSave())
			return false;
		if(($this->scenario=='insert' || $this->scenario=='update') &&
				($image=CUploadedFile::getInstance($this,'img_path'))){
            $this->deleteImage(); // старый документ удалим, потому что загружаем новый

        	$strSource = uniqid();
			$image->saveAs(
				Yii::getPathOfAlias('webroot.images').DIRECTORY_SEPARATOR.$strSource.'.'.$image->extensionName);
			$this->img_path=$strSource.'.'.$image->extensionName;

		}
		return true;
	}


	protected function beforeDelete(){
		if(!parent::beforeDelete())
			return false;
		$this->deleteImage(); // удалили модель? удаляем и файл
		return true;
	}


	public function deleteImage(){
		$imagePath=Yii::getPathOfAlias('webroot.images').DIRECTORY_SEPARATOR.$this->img_path;
		if(is_file($imagePath))
			unlink($imagePath);
	}

}
