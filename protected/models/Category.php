<?php

/**
 * This is the model class for table "{{category}}".
 *
 * The followings are the available columns in table '{{category}}':
 * @property integer $id
 * @property string $name
 *
 * The followings are the available model relations:
 * @property Product[] $products
 */
class Category extends CActiveRecord
{
	public function tableName()
	{
		return '{{category}}';
	}


	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name', 'length', 'max'=>128),
			array('id, name', 'safe', 'on'=>'search'),
		);
	}


	public function relations()
	{
		return array(
			'products' => array(self::HAS_MANY, 'Product', 'category_id'),
		);
	}


	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
		);
	}


	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array('criteria'=>$criteria,));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function getMenuItems()
	{
		$Categories = Category::model()->with('products')->findAll();
		$menuItems = [];

		foreach($Categories as $item) {
			$menuItems[] = [
				'label'=>$item->name,
				'url'=>['/product/index', 'category_id'=>$item->id]
			];
		}
		return $menuItems;
	}

}

