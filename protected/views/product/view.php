<?php
/* @var $this ProductController */
/* @var $model Product */

// $Category=Category::model()->findByPk($category_id);
$this->breadcrumbs=array(
	'Products' => array('index', 'category_id'=>$model->category_id),
	$model->name,
);

$this->menu=array(
	array(	'label'=>'Обзор продуктов',
			'url'=>array('index', 'category_id'=>$model->category_id),
	),
	array(	'label'=>'Создать Продукт',
			'url'=>array('update', 'id'=> null, 'category_id'=>$model->category_id),
			'visible'=>Yii::app()->user->name == 'admin'
	),
	array(	'label'=>'Обновить Продукт',
			'url'=>array('update', 'id'=>$model->id, 'category_id'=>$model->category_id),
			'visible'=>Yii::app()->user->name == 'admin'
	),
	array(	'label'=>'Добавить фото',
			'url'=>array('/foto/update', 'product_id'=>$model->id, 'id'=>null),
			'visible'=>Yii::app()->user->name == 'admin'
	),
	array(	'label'=>'Удалить Продукт',
			'url'=>'#',
			'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),
								'confirm'=>'Are you sure you want to delete this item?',
							),
			'visible'=>Yii::app()->user->name == 'admin'
	),

		array('label'=>'Manage Product', 'url'=>array('admin')),
);
?>

<h1>View Product #<?php echo $model->id; ?></h1>

<?php if (Yii::app()->user->name == 'admin'){
	$this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>array(
			'id',
			'category_id',
			'name',
			'description',
			'price',
			'sort',
		)));
}else{
	$this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>array(
			'name',
			'description',
			'price',
		)));
}?>

<?php if (Yii::app()->user->name == 'admin'){
	$this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$model->fotoProvider(),
		'itemView'=>'/foto/_viewAdmin',
	));
}else{
	$this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$model->fotoProvider(),
		'itemView'=>'/foto/_viewUser',
	));
}?>


