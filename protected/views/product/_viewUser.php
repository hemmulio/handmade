<?php
/* @var $this ProductController */
/* @var $data Product */
?>

	<h4><?php echo CHtml::encode($data->name); ?></h4>
<ul class="thumbnails">
	<?php foreach ($data->fotos as $foto) {
        echo '<li class="span2">';
		echo CHtml::link(CHtml::image("../../../images/$foto->img_path",
			'this is alt tag of image',
			 array(
				'style' => 'border:1px solid #eee; height:140px !important;',
			)
		),"../../../images/$foto->img_path",['data-lightbox'=>"$foto->img_path",]
		);
        echo '</li>';

    } ?>
</ul>

<div class="row">
    <div class="span2">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'Заказать',
            'type'=>'primary',
            'htmlOptions'=>[
                'class'=>'input-block-level btn btn-info',
                'data-toggle'=>'modal',
                'data-target'=>"#product_modal_$data->id",
            ]
        )); ?>
    </div>
    <div class="span10">
        <?php echo CHtml::encode($data->description); ?>

        <b><?php echo CHtml::encode($data->getAttributeLabel('Цена')); ?>:</b>
        <?php echo CHtml::encode($data->price); ?>
    </div>

</div>


<hr>


<?php $this->beginWidget('bootstrap.widgets.TbModal', [
	'htmlOptions'=>[
		'id'=>"product_modal_$data->id"
	]
]); ?>

	<div class="modal-header">
	    <a class="close" data-dismiss="modal">&times;</a>
	    <h3>Заказ</h3>
	</div>
	<div class="modal-body">
		<?php $this->renderPartial('application.views.order._form',
			array('model'=>new Order, 'product_id'=>$data->id)
		); ?>
	</div>

	<div class="modal-footer">
	    <?php $this->widget('bootstrap.widgets.TbButton', array(
	        'label'=>'Зарыть',
	        'url'=>'#',
	        'htmlOptions'=>array('data-dismiss'=>'modal'),
	    )); ?>
	</div>

<?php $this->endWidget(); ?>

<?php if(Yii::app()->session['order']) :?>
<div id="1212" class="modal fade in">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
<!--         <h4 class="modal-title">Modal title</h4>
 -->      </div>
      <div class="modal-body">
        <p>Ваш заказ отправлен, вскоре мы с Вами свяжемся!!!&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="$('#1212').addClass('hide')">Close</button>
        <?php Yii::app()->session['order'] = false; ?>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php endif ?>
