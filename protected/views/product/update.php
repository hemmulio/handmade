<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	'Products'=>array('index', 'category_id'=>$model->category_id),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Обзор продуктов',
			'url'=>array('index', 'category_id'=>$model->category_id),
	),
	array('label'=>'Управление Продуктами',
		'url'=>array('admin'),
		'visible'=>Yii::app()->user->name == 'admin',
	),
);
?>

<h1>Update Product <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
