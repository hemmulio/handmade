<?php
/* @var $this ProductController */
/* @var $dataProvider CActiveDataProvider */

$this->layout = '//layouts/column2';


$this->menu=array(
	array(	'label'=>'Обновить категорию',
			'url'=>array('/category/update', 'id'=>$category_id),
			'visible'=>Yii::app()->user->name == 'admin',
	),
	array(	'label'=>'Удалить категорию',
			'url'=>'#',
			'linkOptions'=>array(	'submit'=>array('/category/delete','id'=>$category_id),
									'confirm'=>'Are you sure you want to delete this item?',
							),
			'visible'=>Yii::app()->user->name == 'admin',
	),
	array(	'label'=>'Создать Продукт',
			'url'=>array('update', 'id'=> null, 'category_id'=>$category_id),
			'visible'=>Yii::app()->user->name == 'admin'
	),
	array(	'label'=>'Управление продуктами', 'url'=>array('admin'),
			'visible'=>Yii::app()->user->name == 'admin',
	),
);
?>
<div class="row">
    <div class="span12 goods">
    <?php
        if (Yii::app()->user->name == 'admin'){
            $this->widget('zii.widgets.CListView', array(
                'dataProvider'=>$dataProvider,
                'itemView'=>'_viewAdmin',
                'summaryText' => '',
            ));
        }else{
            $this->widget('zii.widgets.CListView', array(
                'dataProvider'=>$dataProvider,
                'itemView'=>'_viewUser',
                'summaryText' => '',
            ));
        }
     ?>
    </div>
</div>