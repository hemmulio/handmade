<?php
/* @var $this OrderController */
/* @var $model Order */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'order-form',
	'action'=>'/order/update',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
	'clientOptions' => array(
		'validateOnSubmit'=>true,
        // 'validateOnChange'=>true,
        // 'validateOnType'=>false,
    ),

)); ?>

	<p class="note">Поля отмеченные  <span class="required">*</span>  обязательны к заполнению.</p>

	<?php echo $form->errorSummary($model); ?>
		<?php echo $form->hiddenField($model,'product_id', ['value' => $product_id]); ?>
		<?php echo $form->hiddenField($model,'create_time', ['value' => date("Y-m-d H:i:s", time())]); ?>
		<?php echo $form->hiddenField($model,'ip', ['value' => CHttpRequest::getUserHostAddress()]); ?>
		<?php echo $form->hiddenField($model,'ref_url', ['value' => CHttpRequest::getUrlReferrer()]); ?>

        <?php echo $form->labelEx($model,'Ваше имя'); ?>
        <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->error($model,'name'); ?>

        <?php echo $form->labelEx($model,'Телефон'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'phone'); ?>

		<?php echo $form->labelEx($model,'Комментарий'); ?>
		<?php echo $form->textArea($model,'client_comment',array('rows'=>'3', 'cols'=>50)); ?>
		<?php echo $form->error($model,'client_comment'); ?>

		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'email'); ?>
 	<div>
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Заказать' : 'Save'); ?>
	</div>
<?php $this->endWidget(); ?>

</div><!-- form -->

