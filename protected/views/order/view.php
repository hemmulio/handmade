<?php
/* @var $this OrderController */
/* @var $model Order */

$this->breadcrumbs=array(
	'Orders'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Order', 'url'=>array('index')),
	array('label'=>'Create Order', 'url'=>array('create')),
	array('label'=>'Update Order', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Order',
		'url'=>'#',
		'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),
			'confirm'=>'Are you sure you want to delete this item?'
		),
		'visible'=>Yii::app()->user->name == 'admin',
	),
	array('label'=>'Управление заказами',
		'url'=>array('admin'),
		'visible'=>Yii::app()->user->name == 'admin',
	),
);
?>

<h1>View Order #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'product_id',
		'create_time',
		'phone',
		'name',
		'client_comment',
		'email',
		'ip',
		'ref_url',
		'admin_comment',
	),
)); ?>
