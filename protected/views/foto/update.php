<?php
/* @var $this FotoController */
/* @var $model Foto */

$this->menu=array(
	array(	'label'=>'Удалить фото', 'url'=>'#',
			'linkOptions'=>array('submit'=>array('delete',
								'id'=>$model->id),
								'confirm'=>'Are you sure you want to delete this item?',
								'params'=>array(
									'returnUrl'=>$this->createUrl(
										'/product/view', array(
										'id'=>$model->product_id)
									)
								),
							),
		),
);
?>

<h1>Update Foto <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
