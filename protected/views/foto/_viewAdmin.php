<?php
/* @var $this FotoController */
/* @var $data Foto */
?>

<div class="view">

	<?php echo CHtml::image("../../../images/$data->img_path",
		'this is alt tag of image',
		 array(
		'width'=>'100px',
		'height'=>'100px',
		'align' => 'left',
		'style' => 'padding:0 10px 0 0;'
		)); ?>


	<b><?php echo CHtml::encode($data->getAttributeLabel('img_path')); ?>:</b>
	<?php echo CHtml::encode($data->img_path); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sort')); ?>:</b>
	<?php echo CHtml::encode($data->sort); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<?php echo CHtml::link('редактировать фото', array('/foto/update', 'product_id'=>$data->product_id, 'id'=>$data->id)); ?>
	<br />

</div>

