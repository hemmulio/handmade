<?php
/* @var $this FotoController */
/* @var $data Foto */
?>

<div class="view">

	<?php echo CHtml::image("../../../images/$data->img_path",
		'this is alt tag of image',
		 array(
		'width'=>'70px',
		'height'=>'70px',
		'align' => 'left',
		'style' => 'padding:0 10px 0 0;'
		)); ?>



	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<br />

</div>

