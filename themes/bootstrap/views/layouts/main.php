<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

<!--    <link rel="stylesheet" type="text/css" href="--><?php //echo Yii::app()->theme->baseUrl; ?><!--/css/styles.css" />-->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap_flat.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" >

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php Yii::app()->bootstrap->register(); ?>

	<link href="<?php echo Yii::app()->baseUrl; ?>/lightbox/css/lightbox.css" rel="stylesheet" />

</head>

<body>
<div id="wrap" class="container">

    <header class="jumbotron subhead" id="overview">
        <div class="row">
            <div class="span6">
                <h1>HandMade</h1>
                <p class="muted">Сделано руками с большой любовью</p>
            </div>
        </div>
        <div class="subnav">
            <?php $this->widget('bootstrap.widgets.TbMenu', array(
                'htmlOptions'=>[
                    'class'=>'nav nav-pills',
                ],
                'items'=>array(
                    array('label'=>'Блокноты', 'url'=>array('/product/index', 'category_id'=> 1)),
                    array('label'=>'Сумки', 'url'=>array('/product/index', 'category_id'=> 2)),
                    array('label'=>'Открытки', 'url'=>array('/product/index', 'category_id'=> 3)),
                    array('label'=>'Щеночки', 'url'=>array('/product/index', 'category_id'=> 5)),
                    array('label'=>'Заказы', 'url'=>array('/order/admin'), 'visible'=>Yii::app()->user->name == 'admin'),
                    array('label'=>'Об авторе', 'url'=>array('/site/page', 'view'=>'about')),
                    array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>'Управление категориями', 'url'=>array('/category/index'), 'visible'=>Yii::app()->user->name == 'admin'),
                ),));
            ?>
        </div>
    </header>
    <?php echo $content; ?>
</div><!-- wrap -->




<footer id="footer">
    <div class="container">
        <p class="pull-right">
            <a href="#top">Back to top</a>
        </p>

        <div class="links">
            <a href="#">ВКонтакте</a>&emsp;
            <a href="#">Другая социальная сеть</a>
        </div>
    </div>
</footer>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-2.1.1.min.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo Yii::app()->baseUrl; ?>/lightbox/js/lightbox.min.js"></script>

</body>
</html>
